The piHAT serves as a temperature sensor and emergency power supply for refrigeration systems.

Operation:
The piHAT is to be mounted on the interior of the target refrigeration system. It activates when two conditions are met - mains power is off, and the temperature has risen above a suitable value for a refrigerated environment. In this event, the piHAT battery bank discharges power to the refrigeration system, so that the temperature is restored to below the desired value. It then stops supplying power until both conditions are met again.
